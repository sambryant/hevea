use hyper::{Body, Response};

pub fn empty_response(code: u16) -> Response<Body> {
  Response::builder()
    .status(code)
    .body(Body::from(""))
    .unwrap()
}

pub fn error_response(code: u16, msg: &str) -> Response<Body> {
  Response::builder()
    .status(code)
    .body(Body::from(""))
    .unwrap()
}
